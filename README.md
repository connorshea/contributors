# Contributors

A static site for tracking contributor data. Lightly based on [Rails Contributors](https://github.com/rails/rails-contributors).

Goals:
- Take data from repositories and create pages tracking info like
- Deploy site on a daily basis using pipeline schedules?
- Generates graphs of contributor data over time.
- All static for simplicity's sake.
- For GitLab use, but can be modified to be used by other open source projects.

Tools:
- Ruby 2.5
- Jekyll
- SCSS
- Probably other stuff
